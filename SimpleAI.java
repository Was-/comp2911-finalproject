import java.awt.Color;
import java.util.Random;


public class SimpleAI implements Player{

	private Board board;
	private int playerID;
	private Color color;

	public SimpleAI (int playerID, Color color, Board board) {
		this.playerID = playerID;
		this.color = color;
		this.board = board;
	}

	@Override
	public int getID() {
		return playerID;
	}

	@Override
	public Color getColour() {
		return color;
	}
	
	public int getMove() {
		return simpleAI();
	}

	public int simpleAI() {
		//int i,j,count2,count1;
		//int moved = 0;
		Color playerColor = Color.RED;
		Color aiColor = Color.YELLOW;
		Color Blank = Color.WHITE;
		Random rn = new Random();
		int randomMove = rn.nextInt(6) + 1;
		
		
		/////////////////////////////////////// BLOCK PLAYER
		
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				
				// AI check if player got 3 vertical
				if(y+3<6&&board.getBoard().get(y).get(x).getColor()==playerColor
						&&board.getBoard().get(y+1).get(x).getColor()==playerColor
						&&board.getBoard().get(y+2).get(x).getColor()==playerColor
						&&board.getBoard().get(y+3).get(x).getColor()==Blank
						){
						return(x);
					}
			}
		}
		
		
		
		
		
		/////////////////////////////////////////// AI itself win
		// AI itself win vertical 3 (possible)
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				
				if(		y+3<6
						&&board.getBoard().get(y).get(x).getColor()==aiColor
						&&board.getBoard().get(y+1).get(x).getColor()==aiColor
						&&board.getBoard().get(y+2).get(x).getColor()==aiColor
						&&board.getBoard().get(y+3).get(x).getColor()==Blank){
					return(x);
				}
			}
		}
		// AI itself win horizontal 3 XXX  RIGHT
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				
				if(		x+3<7
						&&board.getBoard().get(y).get(x).getColor()==aiColor
						&&board.getBoard().get(y).get(x+1).getColor()==aiColor
						&&board.getBoard().get(y).get(x+2).getColor()==aiColor
						){
					return(x+3);
			}
			}
		}
		// AI itself win horizontal 3 LEFT
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
			
			if(		x+2<7&&x-1>=0
					&&board.getBoard().get(y).get(x).getColor()==aiColor
					&&board.getBoard().get(y).get(x+1).getColor()==aiColor
					&&board.getBoard().get(y).get(x+2).getColor()==aiColor
					){
				return(x-1);
			}
		}
	}

		/////////////////////////////////////// BLOCK PLAYER
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				
				
				//AI check if player got 3 horizontal ON BASE
				if(		x+2<7
						&&board.getBoard().get(0).get(x).getColor()==playerColor
						&&board.getBoard().get(0).get(x+1).getColor()==playerColor
						&&board.getBoard().get(0).get(x+2).getColor()==playerColor
						){
					
					if (x+3<7
							&&board.getBoard().get(y).get(x+3).getColor()==Blank){
						return(x+3);
					}
					else if(x-3>=0
							&&board.getBoard().get(y).get(x-1).getColor()==Blank){
						return(x-1);
					}
				}
				
			}
		}
		
		///////////////////////////////////// PLAYER TRAP
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
		
		// AI check if player make a trap O O X O  ON BASE
		if(		x+3<7
				&&board.getBoard().get(0).get(x).getColor()==playerColor
				&&board.getBoard().get(0).get(x+1).getColor()==playerColor
				&&board.getBoard().get(0).get(x+2).getColor()==Blank
				&&board.getBoard().get(0).get(x+3).getColor()==playerColor){
			return x+2;
		}
			}
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
		// AI check if player make a trap O X O O ON BASE
		if(		x+3<7
				&&board.getBoard().get(0).get(x).getColor()==playerColor
				&&board.getBoard().get(0).get(x+1).getColor()==Blank
				&&board.getBoard().get(0).get(x+2).getColor()==playerColor
				&&board.getBoard().get(0).get(x+3).getColor()==playerColor){
			return x+1;
		}
			}
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
		
		// AI check if player make a trap O O X O  NOT ON BASE
		if(		x+3<7&&y-1>=0
				&&board.getBoard().get(y).get(x).getColor()==playerColor
				&&board.getBoard().get(y).get(x+1).getColor()==playerColor
				&&board.getBoard().get(y).get(x+2).getColor()==Blank
				&&board.getBoard().get(y).get(x+3).getColor()==playerColor
				&&board.getBoard().get(y-1).get(x+2).getColor()!=Blank){
			return x+2;
		}
			}
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
		// AI check if player make a trap O X O O NOT ON BASE
		if(		x+3<7&&y-1>=0
				&&board.getBoard().get(y).get(x).getColor()==playerColor
				&&board.getBoard().get(y).get(x+1).getColor()==Blank
				&&board.getBoard().get(y).get(x+2).getColor()==playerColor
				&&board.getBoard().get(y).get(x+3).getColor()==playerColor
				&&board.getBoard().get(y-1).get(x+1).getColor()!=Blank){
			return x+1;
		}
			}
		}
		/////////// BLOCK PLAYER HORIZONTAL
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				
				//AI check if player got 3 horizontal
				if(		x+2<7
						&&board.getBoard().get(y).get(x).getColor()==playerColor
						&&board.getBoard().get(y).get(x+1).getColor()==playerColor
						&&board.getBoard().get(y).get(x+2).getColor()==playerColor
						){
					
					if (x+3<7&&y-1>=0
							&&board.getBoard().get(y-1).get(x+3).getColor()!=Blank
							&&board.getBoard().get(y).get(x+3).getColor()==Blank){
						return(x+3);
					}
					else if(x-3>=0&&y-1>=0
							&&board.getBoard().get(y-1).get(x-3).getColor()!=Blank
							&&board.getBoard().get(y).get(x-3).getColor()==Blank){
						return(x-3);
					}
				}
			}
		}
			

		
		
		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       X        3 X
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.getBoard().get(y).get(x).getColor()==playerColor
						&&board.getBoard().get(y+1).get(x+1).getColor()==playerColor
						&&board.getBoard().get(y+2).get(x+2).getColor()==playerColor
						&&board.getBoard().get(y+3).get(x+3).getColor()==Blank
						&&board.getBoard().get(y+2).get(x+3).getColor()!=Blank){
					return(x+3);		
			}
		}
			
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.getBoard().get(y).get(x).getColor()==playerColor
						&&board.getBoard().get(y+1).get(x-1).getColor()==playerColor
						&&board.getBoard().get(y+2).get(x-2).getColor()==playerColor
						&&board.getBoard().get(y+3).get(x-3).getColor()==Blank
						&&board.getBoard().get(y+2).get(x-3).getColor()!=Blank){
					return(x-3);
				
			}
		}
		}
		
		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       O        3 O
		////////// 2     X          2   X
		////////// 1   O            1     O
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.getBoard().get(y).get(x).getColor()==playerColor
						&&board.getBoard().get(y+1).get(x+1).getColor()==playerColor
						&&board.getBoard().get(y+3).get(x+3).getColor()==playerColor
						&&board.getBoard().get(y+2).get(x+2).getColor()==Blank
						&&board.getBoard().get(y+1).get(x+2).getColor()!=Blank){
					return(x+2);		
			}
		}
			
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.getBoard().get(y).get(x).getColor()==playerColor
						&&board.getBoard().get(y+1).get(x-1).getColor()==playerColor
						&&board.getBoard().get(y+3).get(x-3).getColor()==playerColor
						&&board.getBoard().get(y+2).get(x-2).getColor()==Blank
						&&board.getBoard().get(y+1).get(x-2).getColor()!=Blank){
					return(x-2);
				
			}
		}
		}
		
		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   X            1     X
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.getBoard().get(y).get(x).getColor()==playerColor
						&&board.getBoard().get(y+2).get(x+2).getColor()==playerColor
						&&board.getBoard().get(y+3).get(x+3).getColor()==playerColor
						&&board.getBoard().get(y+1).get(x+1).getColor()==Blank
						&&board.getBoard().get(y).get(x+1).getColor()!=Blank){
					return(x+1);		
			}
		}
			
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.getBoard().get(y).get(x).getColor()==playerColor
						&&board.getBoard().get(y+2).get(x-2).getColor()==playerColor
						&&board.getBoard().get(y+3).get(x-3).getColor()==playerColor
						&&board.getBoard().get(y+1).get(x-1).getColor()==Blank
						&&board.getBoard().get(y).get(x-1).getColor()!=Blank){
					return(x-1);
				
			}
		}
		}
		
		/////////// BLOCK PLAYER DIAGONAL  TRAP (base case)
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 X              0       X
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.getBoard().get(y+1).get(x+1).getColor()==playerColor
						&&board.getBoard().get(y+2).get(x+2).getColor()==playerColor
						&&board.getBoard().get(y+3).get(x+3).getColor()==playerColor
						&&board.getBoard().get(0).get(x).getColor()==Blank
						){
					return(x);		
			}
		}
			
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.getBoard().get(y+1).get(x-1).getColor()==playerColor
						&&board.getBoard().get(y+2).get(x-2).getColor()==playerColor
						&&board.getBoard().get(y+3).get(x-3).getColor()==playerColor
						&&board.getBoard().get(0).get(x).getColor()==Blank
						){
					return(x);
				
			}
		}
		}
		///////////  BLOCK PLAYER DIAGONAL  TRAP 
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 X              0       X
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.getBoard().get(y+1).get(x+1).getColor()==playerColor
						&&board.getBoard().get(y+2).get(x+2).getColor()==playerColor
						&&board.getBoard().get(y+3).get(x+3).getColor()==playerColor
						&&board.getBoard().get(y).get(x).getColor()==Blank
						){
					return(x);		
			}
		}
			
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.getBoard().get(y+1).get(x-1).getColor()==playerColor
						&&board.getBoard().get(y+2).get(x-2).getColor()==playerColor
						&&board.getBoard().get(y+3).get(x-3).getColor()==playerColor
						&&board.getBoard().get(y).get(x).getColor()==Blank
						){
					return(x);
				
			}
		}
		}
				/////////////////////////////////////////// BLOCK PLAYER 2 HEAD SNAKE
		
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				// Check Player got 2 head snake horizontal IN BASE 
				if(		x+1<7
						&&board.getBoard().get(0).get(x).getColor()==playerColor
						&&board.getBoard().get(0).get(x+1).getColor()==playerColor
						){
					////// LEFT
					if(x-1>=0&&board.getBoard().get(y).get(x-1).getColor()==Blank){
						return(x-1);
					}
					////// RIGHT
					else if(x+2<7&&board.getBoard().get(y).get(x+2).getColor()==Blank){
						return(x+2);
					}
				}
				
				// Check Player got 2 horizontal NOT IN BASE 
				if(		x+1<7
						&&board.getBoard().get(y).get(x).getColor()==playerColor
						&&board.getBoard().get(y).get(x+1).getColor()==playerColor
						){
					////// LEFT
					if(x-1>=0&&y-1>=0&&x+1<7
							&&board.getBoard().get(y).get(x-1).getColor()==Blank
							&&board.getBoard().get(y-1).get(x-1).getColor()!=Blank
							&&board.getBoard().get(y-1).get(x+1).getColor()!=Blank){
						return(x-1);
					}
					////// RIGHT
//					else if(x+2<7&&board.getBoard().get(y).get(x+2).getColor()==Blank
//							&&board.getBoard().get(y).get(x-1).getColor()==Blank
//							&&board.getBoard().get(y).get(x-1).getColor()==Blank){
//						return(x+2);
//					}
				}
				
			}
		}
		
				////////////////////////////////////////// AI diagonal win
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.getBoard().get(y).get(x).getColor()==aiColor
						&&board.getBoard().get(y+1).get(x+1).getColor()==aiColor
						&&board.getBoard().get(y+2).get(x+2).getColor()==aiColor
						&&board.getBoard().get(y+2).get(x+3).getColor()!=Blank
						&&board.getBoard().get(y+3).get(x+3).getColor()==Blank){
					return(x+3);
				
			}
		}
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.getBoard().get(y).get(x).getColor()==aiColor
						&&board.getBoard().get(y+1).get(x-1).getColor()==aiColor
						&&board.getBoard().get(y+2).get(x-2).getColor()==aiColor
						&&board.getBoard().get(y+2).get(x-3).getColor()!=Blank
						&&board.getBoard().get(y+3).get(x-3).getColor()==Blank){
					return(x-3);
				
			}
		}
		}

		///////////////////////////////////// AI connect 
		
				for (int x=0;x<7;x++){
					for(int y=0;y<6;y++){

				// AI try to connect it's counters (2) Vertical
				if(		y+3<6
						&&board.getBoard().get(y).get(x).getColor()==aiColor
						&&board.getBoard().get(y+1).get(x).getColor()==aiColor
						&&board.getBoard().get(y+2).get(x).getColor()==Blank
						&&board.getBoard().get(y+3).get(x).getColor()==Blank
						){
								return(x);
						}
				
				
				// AI try to connect it's counters (2) Horizontal base RIGHT
				if(		x+3<7
						&&board.getBoard().get(0).get(x).getColor()==aiColor
						&&board.getBoard().get(0).get(x+1).getColor()==aiColor
						&&board.getBoard().get(0).get(x+2).getColor()==Blank
						&&board.getBoard().get(0).get(x+3).getColor()==Blank)
						{
							return x+2;
						}
				if(		x+3<7  && x-2>=0//LEFT
						&&board.getBoard().get(0).get(x).getColor()==aiColor
						&&board.getBoard().get(0).get(x+1).getColor()==aiColor
						&&board.getBoard().get(0).get(x-1).getColor()==Blank
						&&board.getBoard().get(0).get(x-2).getColor()==Blank)
						{
							return x-1;
						}
					}
				}
				
				for (int x=0;x<7;x++){
					for(int y=1;y<6;y++){
				
				if(		x+1<7  && x-2>=0 //LEFT no base
						&&board.getBoard().get(y).get(x).getColor()==aiColor
						&&board.getBoard().get(y).get(x+1).getColor()==aiColor
						&&board.getBoard().get(y-1).get(x-1).getColor()!=Blank
						&&board.getBoard().get(y).get(x-1).getColor()==Blank
						&&board.getBoard().get(y).get(x-2).getColor()==Blank)
						{
							return x-1;
						}
					}
				}
				for (int x=0;x<7;x++){
					for(int y=1;y<6;y++){
				
				if(		x+3<7  && x-2>=0//right no base
						&&board.getBoard().get(y).get(x).getColor()==aiColor
						&&board.getBoard().get(y).get(x+1).getColor()==aiColor
						&&board.getBoard().get(y-1).get(x+2).getColor()!=Blank
						&&board.getBoard().get(y).get(x+2).getColor()==Blank
						&&board.getBoard().get(y).get(x+3).getColor()==Blank)
						{
							return x+2;
						}

					}
				}

		return randomMove;
	}
}
		
		
