import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;



public class MainWindow {
	
	public enum Mode {STANDARD, INVISIBLE}
	
	private JButton startGame;
	private JButton specialModes;
	private JButton instructions;
	private JButton exitGame;
	private JPanel left;
	private JPanel right;
	private JPanel center;
	private JPanel outerMost;
	private JPanel title;
	private JPanel body;
	
	
	private JFrame mainFrame;
	private JPanel gamePanel;
	private JPanel mainMenuPanel;
	private InstructionPanel insPanel;
	private GameEngine gameManager;
	
	

	/**
	 * Method to bootstrap the main frame
	 * @param args
	 */
	
	public static void main(String[] args) {
		
		final MainWindow mw = new MainWindow();
		
		// display the main window in a different thread.
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	mw.display();
            }
        });
	}


	/**
	 * Constructor for the main window.
	 */
	public MainWindow () {
		
		gameManager = new GameEngine();
		gameManager.createNewBoard();
		//gameState = gameManager.getGameBoard();
		
		mainFrame = new JFrame("Connect 4 Demo");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setResizable(true);
		mainFrame.setMinimumSize(new Dimension(650, 450));
		mainFrame.setPreferredSize(new Dimension(650, 450));		
		
		mainMenuPanel = new JPanel();
		mainMenuPanel.setPreferredSize(new Dimension(mainFrame.getWidth(), mainFrame.getHeight()));
		mainMenuPanel.setLayout(new BoxLayout(mainMenuPanel, BoxLayout.PAGE_AXIS));
		//this.setBackground(new Color(0, 200, 255));
		body = new JPanel(new GridLayout(1, 3));
		body.setBackground(Color.DARK_GRAY);
		title = new JPanel();
		title.setBackground(Color.DARK_GRAY);
		title.setPreferredSize(new Dimension(mainFrame.getWidth(), mainFrame.getHeight()/20));
		//body.setPreferredSize(new Dimension(mainFrame.getWidth(), 200));

		
		
		mainMenuPanel.add(title, BorderLayout.NORTH);
		mainMenuPanel.add(body, BorderLayout.CENTER);	
		left = new JPanel();
		center = new JPanel();
		right = new JPanel(new GridLayout(1, 15));
		body.add(left);		
		left.setBackground(Color.DARK_GRAY);

		body.add(center);
		body.add(right);
		right.setBackground(Color.DARK_GRAY);
		
		try {
			BufferedImage buttonIcon = ImageIO.read(new File("startGame.png"));
			startGame = new JButton(new ImageIcon(buttonIcon));
			startGame.setBorder(BorderFactory.createEmptyBorder());
			startGame.setContentAreaFilled(false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		try {
			BufferedImage hello = ImageIO.read(new File("specialModes.png"));
			specialModes = new JButton(new ImageIcon(hello));
			specialModes.setBorder(BorderFactory.createEmptyBorder());
			specialModes.setContentAreaFilled(false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		try {
			BufferedImage buttonIcon = ImageIO.read(new File("exitGame.png"));
			exitGame = new JButton(new ImageIcon(buttonIcon));
			exitGame.setBorder(BorderFactory.createEmptyBorder());
			exitGame.setContentAreaFilled(false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			BufferedImage buttonIcon = ImageIO.read(new File("instructions.png"));
			instructions = new JButton(new ImageIcon(buttonIcon));
			instructions.setBorder(BorderFactory.createEmptyBorder());
			instructions.setContentAreaFilled(false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		// center panel setup
		center.setBackground(Color.DARK_GRAY);
		center.setLayout(new GridLayout(10, 3));
		center.add(Box.createRigidArea(new Dimension(1,0)));
		
		
		// Adding Buttons to center panel
		center.add(startGame);
		center.add(Box.createRigidArea(new Dimension(1,0)));
		center.add(specialModes);
		center.add(Box.createRigidArea(new Dimension(1,0)));
		center.add(instructions);
		center.add(Box.createRigidArea(new Dimension(1,0)));
		center.add(exitGame);
		
	   exitGame.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        	System.exit(0);
	        }
	      });
	   
	   

	   startGame.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        	mainFrame.getContentPane().remove(gameManager.getCurrPanel());
	        	gameManager.setCurrPanel(new NumberPlayersPanel(gameManager));
	        	mainFrame.getContentPane().add(gameManager.getCurrPanel(), BorderLayout.CENTER);
	        	mainFrame.getContentPane().validate();

	        }
	      });
	   instructions.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        	mainFrame.getContentPane().remove(gameManager.getCurrPanel());
	        	gameManager.setCurrPanel(new InstructionPanel(gameManager));
	        	mainFrame.getContentPane().add(gameManager.getCurrPanel(), BorderLayout.CENTER);
	        	mainFrame.getContentPane().validate();

	        }
	      });
	   
	   specialModes.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        	mainFrame.getContentPane().remove(gameManager.getCurrPanel());
	        	gameManager.setCurrPanel(new SelectModePanel(gameManager));
	        	mainFrame.getContentPane().add(gameManager.getCurrPanel(), BorderLayout.CENTER);
	        	mainFrame.getContentPane().validate();
	        }
	      });
	   
		gameManager.setMainMenuPanel(mainMenuPanel);
		gameManager.setJFrame(mainFrame);
		gameManager.setMode(0);
	}

	/**
	 * Method to display the main window
	 */
	public void display() {
		mainFrame.getContentPane().add(mainMenuPanel, BorderLayout.CENTER);
		gameManager.setCurrPanel(mainMenuPanel);
		mainFrame.pack();
        mainFrame.setVisible(true);
	}
}
