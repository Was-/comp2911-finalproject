import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MainMenu extends JPanel implements ActionListener {
	private JButton startGame;
	private JButton specialModes;
	private JButton instructions;
	private JButton exitGame;
	private JPanel left;
	private JPanel right;
	private JPanel center;
	private JPanel outerMost;
	private JPanel title;
	private JPanel body;
	
	
	public MainMenu () {
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		//this.setBackground(new Color(0, 200, 255));
		body = new JPanel(new GridLayout(1, 3));
		body.setBackground(new Color(0, 200, 255));
		title = new JPanel();
		title.setBackground(new Color(200, 0, 255));
		title.setPreferredSize(new Dimension(this.getWidth(), this.getHeight()/5));
		body.setPreferredSize(new Dimension(this.getWidth(), 200));
		
		this.add(title, BorderLayout.PAGE_START);
		this.add(body, BorderLayout.PAGE_END);	
		left = new JPanel();
		center = new JPanel();
		right = new JPanel(new GridLayout(1, 15));
		body.add(left);		
		left.setBackground(new Color(255, 255, 0));

		body.add(center);
		body.add(right);
		right.setBackground(new Color(0, 0, 255));
		
		
		
		// center panel setup
		center.setBackground(new Color(0, 200, 255));
		// center.setPreferredSize(new Dimension(body.getHeight(), this.getWidth()));
		center.setLayout(new GridLayout(10, 3));
		center.add(Box.createRigidArea(new Dimension(1,0)));
		
		
		//creating buttons for center panel
		startGame = new JButton("Start Game");
		specialModes = new JButton("Special Modes");
		instructions = new JButton("Instructions");
		exitGame = new JButton("Exit");
		
		// Adding Buttons to center panel
		center.add(startGame);
		center.add(Box.createRigidArea(new Dimension(1,0)));
		center.add(specialModes);
		center.add(Box.createRigidArea(new Dimension(1,0)));
		center.add(instructions);
		center.add(Box.createRigidArea(new Dimension(1,0)));
		center.add(exitGame);
		
	   exitGame.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        	System.exit(0);
	        }
	      });
	}
	
	public Dimension getPreferredSize() {
        return new Dimension(650,450);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

		
}
