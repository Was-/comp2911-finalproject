import java.util.ArrayList;


public class TreeNode {

	private Board b;
	private int value;
	private int depth;
	private int move;
	private TreeNode parent;
	private ArrayList<TreeNode> children;
	
	public TreeNode(Board oldB, TreeNode parent, Integer move, int depth) {
//		System.out.println(move);
//		System.out.println("oldOGB = ");oldB.showBoard();
//		try {
			this.b = (Board) oldB.clone();
//			System.out.println("Yep");
//		} catch (CloneNotSupportedException e) {}
//		System.out.println("move = "+move);
		if (move != -1) {
			this.b.makeMove(move);
			this.move = move;
		}
//		System.out.println("oldB = ");oldB.showBoard();
//		System.out.println("newB = ");this.b.showBoard();
//		
//		this.b = oldB;
//		b.boardRep = new ArrayList<ArrayList<Counter>>(oldB.boardRep);
//		this.b.makeMove(move);
//		System.out.println("oldB = ");oldB.showBoard();
//		System.out.println("newB = ");this.b.showBoard();
		this.value = -1;
		this.parent = parent;	
		this.children = new ArrayList<TreeNode>();
		this.depth = depth;
		
	}
	
	/*public int calculateValue(int move) {
		int val = b.checkForStreak(move);
//		switch(val) {
//		case 1: return 2;
//		case -1: return 1;
//		default: return 0; //val = 0;
//		}
		return val;
	}*/

	public void addChild(TreeNode child) {
//		TreeNode child = new TreeNode(b,this,move,depth+1);
		children.add(child);
	}
	
	public ArrayList<TreeNode> getChildren() { //need
		return children;
	}

	public int getMove() { //need
		return move;
	}
	
	public int getValue() { //need
		return value;
	}
	
	public void setValue(int i) { //need
		value = i;
	}

	public int getDepth() {
		return depth;
	}

	public Board getBoard() {
		return b;
	}
}