import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;


public interface ConnectFour {
		
	public void mouseDragged(MouseEvent arg0);

	public void mouseMoved(MouseEvent e);

	public void mouseClicked(MouseEvent arg0);
	
	public void paint();
	
	public void mouseEntered(MouseEvent e);
	
	public void mouseExited(MouseEvent e);

	public void mousePressed(MouseEvent e);
	
	public void mouseReleased(MouseEvent e);

	public void setDisplayCounter (Color c, int row, int col);
	
	public void keyPressed(KeyEvent arg0);

	public int getCounterDimension ();
	
	public void keyReleased(KeyEvent arg0);

	public Board getGameState ();
	
	public void keyTyped(KeyEvent arg0);
	
	public boolean makeMove(int col, Color c);
	
}
