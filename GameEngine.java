import javax.swing.SwingUtilities;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.util.Random;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class GameEngine {
	//public enum Mode {STANDARD, INVISIBLE};	

	private Board gameBoard;
	private JFrame mainFrame;
	private JPanel mainMenuPanel;
	private JPanel cards;
	private JPanel currPanel; 
	private int modeType;
	private int numPlayers;
	
	public int aiType;
    public Player player1;
    public Player player2;
	
	public GameEngine () {
		cards = new JPanel(new CardLayout());
	}

	public Player getPlayer1() {
        return player1;
    }
    
    public Player getPlayer2() {
        return player2;
    }
	
	public Board createNewBoard() {
		gameBoard = new Board();
	return gameBoard;
	}

	public Board getGameBoard() {
		return gameBoard;
	}
	
	public JPanel getCards () {
		return cards;
	}
	
	public void addCard (JPanel card) {
		cards.add(card);
	}
	
	public JPanel getCurrPanel () {
		return currPanel;
	}
	
	public void setMode (int modeType) {
		this.modeType = modeType;
	}
	
	public int getMode () {
		return modeType;
	}

	public void setAI(int aiType){
        this.aiType = aiType;
    }
    public int getAI () {
        return aiType;
    }
	
	public void setNumHumanPlayers (int numPlayers) {
		this.numPlayers = numPlayers;
	}
	
	public int getNumHumanPlayers() {
		return numPlayers;
	}
	
	public void setMainMenuPanel (JPanel mainMenuPanel) {
		this.mainMenuPanel = mainMenuPanel;
	}
	
	public JPanel getMainMenuPanel () {
		return this.mainMenuPanel;
	}
	
	public void setJFrame (JFrame main) {
		this.mainFrame = main;
	}
	
	public JFrame getJFrame () {
		return mainFrame;
	}
	
	public void setCurrPanel (JPanel currPanel) {
		this.currPanel = currPanel;
	}
	
	
}
