import static org.junit.Assert.*;

import org.junit.*;


public class BoardTest {

	private static final boolean debug = false;
	Board gameBoard;
	
	
	@Test
	public void testGetLastMove() {
		gameBoard = new Board();
		
		//---------------test: 1---------------
		gameBoard.clearBoard();
		
		gameBoard.makeMove(0);
		assertEquals(0, gameBoard.getLastMove());
		gameBoard.makeMove(1);
		assertEquals(1, gameBoard.getLastMove());
		gameBoard.makeMove(1);
		assertEquals(1, gameBoard.getLastMove());
		gameBoard.makeMove(2);
		assertEquals(2, gameBoard.getLastMove());
		gameBoard.makeMove(2);
		assertEquals(2, gameBoard.getLastMove());
		gameBoard.makeMove(3);
		assertEquals(3, gameBoard.getLastMove());
		gameBoard.makeMove(5); 
		assertEquals(5, gameBoard.getLastMove());
		gameBoard.makeMove(3);
		assertEquals(3, gameBoard.getLastMove());
		gameBoard.makeMove(3);
		assertEquals(3, gameBoard.getLastMove());
		gameBoard.makeMove(5);
		gameBoard.makeMove(3);
		gameBoard.makeMove(5);
		gameBoard.makeMove(2);
		assertEquals(2, gameBoard.getLastMove());
		
	}
	
	
	@Test
	public void testMakeMove() {
		gameBoard = new Board();
		
		//---------------test: single column---------------
		gameBoard.clearBoard();
		
		assertTrue(gameBoard.makeMove(0));
		assertTrue(gameBoard.makeMove(0));
		assertTrue(gameBoard.makeMove(0));
		assertTrue(gameBoard.makeMove(0));
		assertTrue(gameBoard.makeMove(0));
		assertTrue(gameBoard.makeMove(0));
		assertFalse(gameBoard.makeMove(0));
		assertTrue(gameBoard.makeMove(1));
		if (debug) gameBoard.showBoard();
	}
	
	
	@Test
	public void testCheckForWin() {

		gameBoard = new Board();
		int i;

		//---------------test: diagonal 1---------------
		gameBoard.clearBoard();
		i = 0;
		gameBoard.makeMove(i);
		if (debug) System.out.println("1");
		assertEquals(0, gameBoard.checkForWin(i));
		i = 1;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 2;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 3;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 2;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 3;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		gameBoard.makeMove(i); 
		assertEquals(0, gameBoard.checkForWin(i));
		i = 5;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 3;
		gameBoard.makeMove(i);
		assertEquals(1, gameBoard.checkForWin(i));
		

		//---------------test: horizontal left---------------
		gameBoard.clearBoard();

		i = 0;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 2;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 3;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 1;
		gameBoard.makeMove(i); 
		assertEquals(1, gameBoard.checkForWin(i));


		//---------------test: diagonal 2------------------
		gameBoard.clearBoard();

		i = 0;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 1;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 2;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 3;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 5;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i)); 
		i = 3;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));              
		i = 5;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 3;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 5;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 2;
		gameBoard.makeMove(i);
		assertEquals(1, gameBoard.checkForWin(i));


		//---------------test: diagonal 3---------------
		gameBoard.clearBoard();

		i = 0;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 1;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 2;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 3;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 2;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 1;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i)); 
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 0;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));              
		i = 3;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		i = 0;
		gameBoard.makeMove(i);
		assertEquals(0, gameBoard.checkForWin(i));
		gameBoard.makeMove(i);
		assertEquals(1, gameBoard.checkForWin(i));


		//---------------test: drawn game---------------
		gameBoard.clearBoard();

		for (int j=0;j<3;j++) {
			if (j == 3) {
				continue;
			}
			for (int k=0;k<3;k++) {
				if (j==4 && k==5) {
					continue;
				}
				//if (debug) System.out.println("i="+i+", j="+j);
				gameBoard.makeMove(j);
				assertEquals(0, gameBoard.checkForWin(j));
				gameBoard.makeMove(j);
				assertEquals(0, gameBoard.checkForWin(j));
				gameBoard.makeMove(6-j);
				assertEquals(0, gameBoard.checkForWin(6-j));
				if (j==2 && k==2) {
					break;
				}
				gameBoard.makeMove(6-j);
				assertEquals(0, gameBoard.checkForWin(6-j));
			}
		}
		for (int k=0;k<6;k++) {
			gameBoard.makeMove(3);
			assertEquals(0, gameBoard.checkForWin(3));
		}
		gameBoard.makeMove(4);
		assertEquals(-1, gameBoard.checkForWin(4));

	}

	@Test
	public void testGetHumanValue3() {
		gameBoard = new Board();
		Board b;
		

		//---------------test 3 horizontal bottom row---------------
		gameBoard.clearBoard();
		
		//initial setup
		gameBoard.makeMove(3);
		gameBoard.makeMove(3);
		gameBoard.makeMove(4);
		gameBoard.makeMove(2);
		gameBoard.makeMove(5);
		
		b = newMoveBoard(0);
		assertEquals(6,b.getValueHuman3());
		b = newMoveBoard(1);
		assertEquals(6,b.getValueHuman3());
		b = newMoveBoard(2);
		assertEquals(6,b.getValueHuman3());
		b = newMoveBoard(3);
		assertEquals(6,b.getValueHuman3());
		b = newMoveBoard(4);
		assertEquals(6,b.getValueHuman3());
		b = newMoveBoard(5);
		assertEquals(6,b.getValueHuman3());
		b = newMoveBoard(6);
		assertEquals(-1,b.getValueHuman3());
		
		//---------------test: diagonal 1---------------
		gameBoard.clearBoard();
		
		//initial setup
		gameBoard.makeMove(0);
		gameBoard.makeMove(1);
		gameBoard.makeMove(1);
		gameBoard.makeMove(2);
		gameBoard.makeMove(2);
		gameBoard.makeMove(3);
		gameBoard.makeMove(2); 
		gameBoard.makeMove(3);
		gameBoard.makeMove(3);              
		gameBoard.makeMove(5);
		
		b = newMoveBoard(0);
		assertEquals(3,b.getValueHuman3());
		b = newMoveBoard(1);
		assertEquals(3,b.getValueHuman3());
		b = newMoveBoard(2);
		assertEquals(3,b.getValueHuman3());
		b = newMoveBoard(3);
		assertEquals(-1,b.getValueHuman3());
		b = newMoveBoard(4);
		assertEquals(3,b.getValueHuman3());
		b = newMoveBoard(5);
		assertEquals(3,b.getValueHuman3());
		b = newMoveBoard(6);
		assertEquals(3,b.getValueHuman3());
		
/*		//---------------test: horizontal left---------------
		gameBoard.clearBoard();

		gameBoard.makeMove(0);
		gameBoard.makeMove(0);
		gameBoard.makeMove(2);
		gameBoard.makeMove(2);
		gameBoard.makeMove(3);
		gameBoard.makeMove(3);

		b = newMoveBoard(0);
		assertEquals(1,b.getValueHuman3());
		b = newMoveBoard(1);
//		assertEquals(-1,b.getValueHuman3());
		b.showBoard();
		b = newMoveBoard(2);
		assertEquals(1,b.getValueHuman3());
		b = newMoveBoard(3);
		assertEquals(1,b.getValueHuman3());
		b = newMoveBoard(4);
		assertEquals(5,b.getValueHuman3());
		b = newMoveBoard(5);
		assertEquals(4,b.getValueHuman3());
		b = newMoveBoard(6);
		assertEquals(1,b.getValueHuman3());*/
		

		//---------------test: vertical------------------
		gameBoard.clearBoard();
		
		gameBoard.makeMove(0);
		gameBoard.makeMove(1);
		gameBoard.makeMove(0);
		gameBoard.makeMove(1);
		gameBoard.makeMove(0);
		gameBoard.makeMove(1);
		
		b = newMoveBoard(0);
		b.showBoard();
		assertEquals(-1,b.getValueHuman3());
		b = newMoveBoard(1);
		assertEquals(0,b.getValueHuman3());
		b = newMoveBoard(2);
		assertEquals(0,b.getValueHuman3());
		b = newMoveBoard(3);
		assertEquals(0,b.getValueHuman3());
		b = newMoveBoard(4);
		assertEquals(0,b.getValueHuman3());
		b = newMoveBoard(5);
		assertEquals(0,b.getValueHuman3());
		b = newMoveBoard(6);
		assertEquals(0,b.getValueHuman3());
		
	}
	
	private Board newMoveBoard(int move) {
		Board b = (Board) gameBoard.clone();
		b.makeMove(move);
		return b;
	}
	
}
