import java.awt.Color;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;


public class IntermediateAI implements Player {

	private static final boolean time = false;
	private static final boolean debug = false;
	private static final boolean debug2 = false;
	private Board b;
	private int playerID;
	private Color color;
//	private TreeNode t;
	
	public IntermediateAI(int playerID, Color color, Board b) {
		this.playerID = playerID;
		this.color = color;
		this.b = b;
//		t = new TreeNode(b,null,-1,0);
//		t.b = b;
		
	}
	
	@Override
	public int getID() {
		return playerID;
	}

	@Override
	public Color getColour() {
		return color;
	}

	@Override
	public int getMove() {
		
		//createTree(t);
		
//		showTree(t);
		
		long startTime = System.nanoTime();
		
		int res = findValue(new TreeNode(b,null,-1,0),0,playerID)[1];
		
		long endTime = System.nanoTime();
		if (time) System.out.println("Took "+(double)(endTime - startTime)/1000000000 + " s");
		Runtime runtime = Runtime.getRuntime();
		long memory = runtime.totalMemory() - runtime.freeMemory();
		if (time) System.out.println("memory used = " + (double)memory/(1024*1024) + " MB");
		
		
//		int res = 0;
		if (debug) System.out.println("this is res: "+res);
		
		return res;
	}
	
	private int[] findValue(TreeNode t, int depth, int player) {
		
		//result is an array to return two values, the value of the tree and the move it corresponds to.
		int[] result = {0,0};

		//recursive base case - stops recursion and calculates value of the current state (TreeNode)
		if (depth == 3) {
			result[1] = t.getMove();
//			int res = new Random().nextInt(10)+1;
//			if (t.getBoard().checkForWin(t.getMove()) == 1) {
//				res = 1000;
//			}
			int res = t.getBoard().getValue();
			if (debug2) System.out.println("This is the value: "+res);
			result[0] = res;
			if (debug2) System.out.println("inner result = "+result[0]);
			if (debug2) t.getBoard().showBoard();
			return result;
		}
		
		//Generate the tree by creating up to 7 child nodes
		for (int i=0;i<7;i++) {
			Board bd = t.getBoard();
			if (t.getBoard().validMove(i)) {
//				t.addChild(i);
				TreeNode child = new TreeNode(t.getBoard(),t,i,depth+1);
				if (child.getBoard().checkForWin(i) == 1) {
					if (debug2) if (depth == 0) System.out.println("Almost");
					if (player == playerID) {
						if (depth == 0) {
							child.setValue(10000);
							System.out.printf("AI0 [move: %d, CWVal: %d]\n",child.getMove(),1);
						} else {
							child.setValue(5000);
						}
						if (debug2) System.out.println("This guy worked: "+child.getMove());
					} else {
						child.setValue(0);
						if (debug2) System.out.println("AI as well!: "+child.getMove());
					}
					if (depth==0) System.out.printf("AI [move: %d, CWVal: %d]\n",child.getMove(),1);
				} else if (depth == 0 && child.getBoard().getValueHuman3() != -1) {
					System.out.printf("HUMAN [move: %d, human: %d]\n",child.getMove(),child.getBoard().getValueHuman3());
					child.setValue(0);
				}
//				t.getChildren().add(child);
				t.addChild(child);
			}
		}
		
		//vals stores the return value from the recursive call
		int vals[] = new int[2];
		
		// 2 cases, if AIs turn we want maximum value and
		// if it is the human's turn, we want the minimum value
		if (player == playerID) {
			result[0] = Integer.MIN_VALUE; //initialize to lowest value so it is always replaced
			for (TreeNode node: t.getChildren()) {
				/*if (depth == 1 && t.getValue() == 5000) {
					vals[0] = t.getValue();
					vals[1] = t.getMove();
					return vals;
				}*/
				if (node.getValue() != -1) {
					vals[0] = node.getValue();
					vals[1] = node.getMove();
				} else {
					vals = findValue(node, depth+1, (player+1)%2); //recursive call
//					node.setValue(vals[0]);
				}
				
				if (vals[0] > result[0]) {
					result[0] = vals[0];
					result[1] = node.getMove();
				}
				/*if (debug)*/ System.out.println(depth+") [result = "+result[0]+", rMove = "+result[1]+"]"
						+" [move = "+vals[1]+", vals = "+vals[0]+"]");
				/*if (depth == 0 && vals[0]==0) {
					return vals;
				}*/
			}
			if (depth == 0) {
				for (TreeNode node: t.getChildren()) {
					System.out.printf("%d. value=%d\n", node.getMove(), node.getValue());
				}
			}
			if (depth == 0) System.out.println("FINAL result = "+result[0]+" MOVE: "+result[1]);
		} else {
			result[0] = Integer.MAX_VALUE; //initialize to highest value so it is always replaced
			for (TreeNode node: t.getChildren()) {
				if (depth > 0 && node.getValue() != -1) {
					vals[0] = node.getValue();
					vals[1] = node.getMove();
				} else {
					vals = findValue(node, depth+1, (player+1)%2); //recursive call
				}
				if (vals[0] < result[0]) {
					result[0] = vals[0];
					result[1] = node.getMove();
				}
				/*if (debug2)*/ System.out.println(depth+") [result = "+result[0]+", rMove = "+result[1]+"]"
						+" [move = "+vals[1]+", vals = "+vals[0]+"]");
				/*if (depth == 0 && vals[0]==0) {
					return vals;
				}*/
			}
			if (depth == 0) System.out.println("MIN!!!! FINAL result = "+result[0]+" MOVE: "+result[1]);
		}
		
		return result;
	}
	
	private void showTree(TreeNode startNode) {
		int depth = 0, counter = 0;
		Queue<TreeNode> queue=new LinkedList<TreeNode>();  
		queue.add(startNode);  
		while(!queue.isEmpty()) {  
			TreeNode tempNode=queue.poll();  
			if (tempNode.getDepth() > depth) {
				System.out.println("");
				depth = tempNode.getDepth();
			}
			if (depth == 1) {
				counter++;
			}
			System.out.println(tempNode.getDepth()+": ");
			tempNode.getBoard().showBoard();
			for (TreeNode childNode: tempNode.getChildren()) {
				queue.add(childNode);	
			}
		} 
		System.out.println("");
		System.out.println(counter);
		
	}
	
	/*
	private void createTree(TreeNode t) {
		if (t.getDepth() == 3) {
			return;
		}
		for (int i=0;i<7;i++) {
			TreeNode child = new TreeNode(t.getBoard(),t,i,t.getDepth()+1);
			t.getChildren().add(child);
		}
		for (TreeNode child : t.getChildren()) {
			createTree(child);
		}
	}
	*/
	
	
	
	/*
	private int[] getValue(TreeNode t, int depth, int player) {
//		return null;
		
		int[] result = {0,0};
		if (depth == 1) {
			//DO RANDOM VALUE STUFF ABOVE
			
			result[1] = t.getMove();
//			if (player != playerID) {
//				result[0] = t.calculateValue(t.getMove());
//			} else {
//				result[0] = -t.calculateValue(t.getMove());
//			}
////			System.out.println(result[0]+" depth="+depth);
//			t.getBoard().showBoard();
			result[0] = t.getValue();
			return result;
		}
		int[] vals;
		int minMax;
//		
//		if (player == playerID) {
//			minMax = 0;
//			for (int i=0;i<7;i++) {
//				if (!t.getBoard().validMove(i)) {
//					continue;
//				}
//				if (t.calculateValue(i) >= minMax) {
//					TreeNode child = new TreeNode(b,t,i,depth+1);
//					t.getChildren().add(child);
//				}
//			}
//		} else {
//			minMax = Integer.MAX_VALUE;
//			for (int i=0;i<7;i++) {
//				if (!t.getBoard().validMove(i)) {
//					continue;
//				}
//				TreeNode child = new TreeNode(b,t,i,depth+1);
//				if (child.getMove() < minMax) {
//					t.getChildren().add(child);
//				}
//			}
//		}
//		
		if (depth == 0) {
		for (int i=0;i<4;i++) {
//			if (t.getBoard().validMove(i)) {
//				TreeNode child = new TreeNode(b,t,i,depth+1);
//				t.getChildren().add(child);
//			}
			
			TreeNode child = new TreeNode(b,t,i,depth+1);
			for (int j=0;i<4;j++) {
				TreeNode subChild = new TreeNode(child.getBoard(),child,i,depth+2);
				subChild.value = (i*j)+j;
				child.getChildren().add(subChild);
			}
			t.getChildren().add(child);
		}
		}
		
		if (player == playerID) {
			minMax = Integer.MIN_VALUE;
			for (TreeNode node: t.getChildren()) {
				vals = getValue(node, depth+1, (player+1)%2);
				if (vals[0] > result[0]) {
					result[0] = vals[0];
					result[1] = vals[1];
				}
//				if (depth == 1) {
//					System.out.println(vals[0]+" depth="+depth);
//				}
			}
//			System.out.println("");
//			System.out.println("depth: "+depth+" minMax: "+minMax);
			System.out.println("Max = "+result[0]);
		} else {
			minMax = Integer.MAX_VALUE;
			for (TreeNode node: t.getChildren()) {
				vals = getValue(node, depth+1, (player+1)%2);
				if (vals[0] < result[0]) {
					result[0] = vals[0];
					result[1] = vals[1];
				}
				if (depth == 1) {
					System.out.println(vals[0]+" depth="+depth);
				}
			}
//			System.out.println("");
//			System.out.println("depth: "+depth+" minMax: "+minMax);
			System.out.println("\nMin = "+result[0]);
		}
		
		return result;
		
	}
	*/
	
	
}