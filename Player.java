import java.awt.Color;


public interface Player {
	
	public int getID();
	public Color getColour();
	public int getMove();
	
}
