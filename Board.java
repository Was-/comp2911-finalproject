import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;
import java.awt.Color;


public class Board implements Cloneable{

	private static final boolean debug = false;
	ArrayList<ArrayList<Counter>> board; //board[row][col] = board[6][7]
	ArrayList<Integer> lowestCounter;
	Stack<Integer> moves = new Stack<Integer>(), redo = new Stack<Integer>();
	
	public Board() {
		board = new ArrayList<ArrayList<Counter>>();
		for (int i=0;i<6;i++) {
			board.add(new ArrayList<Counter>());
			for (int j=0;j<7;j++) {
				board.get(i).add(new Counter(Color.WHITE, i, j));
			}
		}
		lowestCounter = new ArrayList<Integer>(Collections.nCopies(7,0));
	}
	
	public int getRoundNum() {
		return moves.size();
	}
	
	public int getLastMove(){
		if (moves.empty()) return 0;
		return moves.peek();
	}
	
	public ArrayList<ArrayList<Counter>> getBoard () {
		return board;
	}
	
	public Counter getCounter(int col, int row) {
		return board.get(row).get(col);
	}
	
	//takes roundNum-1 to find last player who made a move
	//and not the current player.
	public int findLastPlayerID() {
		return (moves.size() - 1) % 2 + 1;
	}
	
	public void showBoard() {
		for (int i=0;i<6;i++) {
			System.out.print("| ");
			for (int j=0;j<7;j++) {
				if (j == 6) {
					System.out.print(board.get(5-i).get(j)+" |");
				} else {
					System.out.print(board.get(5-i).get(j)+" | ");
				}
			}
			System.out.println("");
		}
	}
	
	public void clearBoard() {
		if (debug) System.out.println("Clearing Board...");
		for (int i=0;i<6;i++) {
			for (int j=0;j<7;j++) {
				board.get(i).get(j).setColor(Color.WHITE);
				
			}
		}
		moves.clear();
		lowestCounter = new ArrayList<Integer>(Collections.nCopies(7,0));
	}
	
	
	
	
	public boolean makeMove(int column) {
		if (!lowestCounter.get(column).equals(6)) {
			int row = lowestCounter.get(column);
			makeValidMove(column);
			lowestCounter.set(column,row+1);
			moves.push(column);
			//System.out.println("win: "+checkForWin(column));
			return true;
		}
		if (debug) System.out.println("invalid");
		return false;
	}
	
	public void makeValidMove (int col) {
		int row = lowestCounter.get(col);
		board.get(row).get(col).setColor(moves.size() % 2 == 0? Color.RED : Color.YELLOW);
		
	}

	public boolean validMove (int column) {
		if (lowestCounter.get(column).equals(6)) {
			return false;
		}
		return true;
	}

	@Override
	public Object clone() {
	    Board cloned = this;
		try {
			cloned = (Board)super.clone();
			cloned.board = new ArrayList<ArrayList<Counter>>();
		    for (int i=0;i<6;i++) {
				cloned.board.add(new ArrayList<Counter>());
				for (int j=0;j<7;j++) {
					cloned.board.get(i).add(new Counter(this.board.get(i).get(j).getColor(),i,j));
				}
			}
		    cloned.lowestCounter = new ArrayList<Integer>(this.lowestCounter);
		    cloned.moves = (Stack<Integer>) moves.clone();
		    cloned.redo = (Stack<Integer>) moves.clone();
		} catch (CloneNotSupportedException e) {}
	    return cloned;
	}

	
	 public int getRow(int col) {
	        return lowestCounter.get(col);
	  }
	
	public int checkForWin(int col) {
		
		int result = 0;
		
		//Check for draw
		if (moves.size() == 42) {
			result = -1;
		}
		
		int row = lowestCounter.get(col);
		if (row != 0) {
			row--;
		}
		Color c = board.get(row).get(col).getColor();
		
		//Checking for vertical win (moving down)
		int count1=1;
		while (count1<4 && (row-count1)>=0 && board.get(row-count1).get(col).getColor() == c) {
			count1 ++;
		}
		if (count1==4){
			result = 1;
		}
		
		//Checking horizontal (left then right)
		count1=1;
		int count2=1;
		while (count1<4 && (col-count1)>=0 && board.get(row).get(col-count1).getColor()==c){
			count1 ++;	
			
		}
		while (count2+count1-1<4 && (col+count2)<7 && board.get(row).get(col+count2).getColor()==c){
			count2 ++;	
		}
		if (count1+count2-1 == 4){
			result = 1;
		}
		
		
		//Checking diagonal 1 (upper left then lower right)
		count1=1;
		count2=1;
		while (count1<4 && (col-count1)>=0 && (row+count1)<6 
				&& board.get(row+count1).get(col-count1).getColor()==c){
			count1 ++;	
		}
		while (count2+count1-1<4 && (col+count2)<7 && (row-count2)>=0 
				&& board.get(row-count2).get(col+count2).getColor()==c){
			count2 ++;	
		}
		if (count1+count2-1 == 4){
			result = 1;
		}
		
		//Checking diagonal 2 (upper right then lower left)
		count1=1;
		count2=1;
		while (count1<4 && (col+count1)<7 && (row+count1)<6 
				&& board.get(row+count1).get(col+count1).getColor()==c){
			count1 ++;	
		}
		while (count2+count1-1<4 && (col-count2)>=0 && (row-count2)>=0 
				&& board.get(row-count2).get(col-count2).getColor()==c){
			count2 ++;	
		}
		if (count1+count2-1 == 4){
			result = 1;
		}
		
		switch (result) {
			case 1: if (debug) System.out.println("Win!!!");break;
			case -1: if (debug) System.out.println("Draw!!!");break;
			default: if (debug) System.out.println("Round: "+ moves.size() +" complete"); break;
		}
		
		return result;
	}
	
	public boolean undo(){
		if (moves.empty()) return false;
		int col = redo.push(moves.pop()), row = lowestCounter.get(col) - 1;
		board.get(row).get(col).setColor(Color.white);
		lowestCounter.set(col,row);
		return true;
	}
	
	public boolean redo(){
		if (redo.isEmpty()) return false;
		makeMove(redo.pop());
		return true;
	}
	
	
	
	public int getValueHuman3(){
		Color playerColor = Color.RED;
		Color Blank = Color.WHITE;

		
		/////////////////////////////////////// BLOCK PLAYER
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				
				
		//AI check if player got 3 horizontal ON BASE
		
		for (int x=0;x<7;x++){
				
				//AI check if player got 3 horizontal ON BASE
				if(		x+3<7
						&&board.get(0).get(x).getColor()==playerColor
						&&board.get(0).get(x+1).getColor()==playerColor
						&&board.get(0).get(x+2).getColor()==playerColor
						&&board.get(0).get(x+3).getColor()==Blank
				)
					return x+3;
			
		}
		
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		x-3>=0
						&&board.get(0).get(x).getColor()==playerColor
						&&board.get(0).get(x-1).getColor()==playerColor
						&&board.get(0).get(x-2).getColor()==playerColor
						&&board.get(0).get(x-3).getColor()==Blank
				)
					return x-3;
			}
		}
		///////////////////////////////////// PLAYER TRAP
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
		
		// AI check if player make a trap O O X O  ON BASE
		if(		x+3<7
				&&board.get(0).get(x).getColor()==playerColor
				&&board.get(0).get(x+1).getColor()==playerColor
				&&board.get(0).get(x+2).getColor()==Blank
				&&board.get(0).get(x+3).getColor()==playerColor){
			return x+2;
		}
			}
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
		// AI check if player make a trap O X O O ON BASE
		if(		x+3<7
				&&board.get(0).get(x).getColor()==playerColor
				&&board.get(0).get(x+1).getColor()==Blank
				&&board.get(0).get(x+2).getColor()==playerColor
				&&board.get(0).get(x+3).getColor()==playerColor){
			return x+1;
		}
			}
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
		
		// AI check if player make a trap O O X O  NOT ON BASE
		if(		x+3<7&&y-1>=0
				&&board.get(y).get(x).getColor()==playerColor
				&&board.get(y).get(x+1).getColor()==playerColor
				&&board.get(y).get(x+2).getColor()==Blank
				&&board.get(y).get(x+3).getColor()==playerColor
				&&board.get(y-1).get(x+2).getColor()!=Blank){
			return x+2;
		}
			}
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
		// AI check if player make a trap O X O O NOT ON BASE
		if(		x+3<7&&y-1>=0
				&&board.get(y).get(x).getColor()==playerColor
				&&board.get(y).get(x+1).getColor()==Blank
				&&board.get(y).get(x+2).getColor()==playerColor
				&&board.get(y).get(x+3).getColor()==playerColor
				&&board.get(y-1).get(x+1).getColor()!=Blank){
			return x+1;
		}
			}
		}
		/////////// BLOCK PLAYER HORIZONTAL
		for (int x=0;x<7;x++){
			for(int y=1;y<6;y++){
				
				//AI check if player got 3 horizontal
				if(		x+3<7
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y).get(x+1).getColor()==playerColor
						&&board.get(y).get(x+2).getColor()==playerColor
						&&board.get(y).get(x+3).getColor()==Blank
						){
						return x+3;
				}
				if(		x-3>=0
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y).get(x-1).getColor()==playerColor
						&&board.get(y).get(x-2).getColor()==playerColor
						&&board.get(y).get(x-3).getColor()==Blank)
				{
					return x-3;
				}
			}
		}
			
		
		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       X        3 X
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y+1).get(x+1).getColor()==playerColor
						&&board.get(y+2).get(x+2).getColor()==playerColor
						&&board.get(y+3).get(x+3).getColor()==Blank
						&&board.get(y+2).get(x+3).getColor()!=Blank){
					return(x+3);		
			}
		}
			
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y+1).get(x-1).getColor()==playerColor
						&&board.get(y+2).get(x-2).getColor()==playerColor
						&&board.get(y+3).get(x-3).getColor()==Blank
						&&board.get(y+2).get(x-3).getColor()!=Blank){
					return(x-3);
				
			}
		}
		}
		
		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       O        3 O
		////////// 2     X          2   X
		////////// 1   O            1     O
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y+1).get(x+1).getColor()==playerColor
						&&board.get(y+3).get(x+3).getColor()==playerColor
						&&board.get(y+2).get(x+2).getColor()==Blank
						&&board.get(y+1).get(x+2).getColor()!=Blank){
					return(x+2);		
			}
		}
			
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y+1).get(x-1).getColor()==playerColor
						&&board.get(y+3).get(x-3).getColor()==playerColor
						&&board.get(y+2).get(x-2).getColor()==Blank
						&&board.get(y+1).get(x-2).getColor()!=Blank){
					return(x-2);
				
			}
		}
		}
		
		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   X            1     X
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y+2).get(x+2).getColor()==playerColor
						&&board.get(y+3).get(x+3).getColor()==playerColor
						&&board.get(y+1).get(x+1).getColor()==Blank
						&&board.get(y).get(x+1).getColor()!=Blank){
					return(x+1);		
			}
		}
			
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y+2).get(x-2).getColor()==playerColor
						&&board.get(y+3).get(x-3).getColor()==playerColor
						&&board.get(y+1).get(x-1).getColor()==Blank
						&&board.get(y).get(x-1).getColor()!=Blank){
					return(x-1);
				
			}
		}
		}
		
		/////////// BLOCK PLAYER DIAGONAL  TRAP (base case)
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 X              0       X
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y+1).get(x+1).getColor()==playerColor
						&&board.get(y+2).get(x+2).getColor()==playerColor
						&&board.get(y+3).get(x+3).getColor()==playerColor
						&&board.get(0).get(x).getColor()==Blank
						){
					return(x);		
			}
		}
			
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y+1).get(x-1).getColor()==playerColor
						&&board.get(y+2).get(x-2).getColor()==playerColor
						&&board.get(y+3).get(x-3).getColor()==playerColor
						&&board.get(0).get(x).getColor()==Blank
						){
					return(x);
				
			}
		}
		}
		
		
		
		///////////  BLOCK PLAYER DIAGONAL  TRAP 
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 X              0       X
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y+1).get(x+1).getColor()==playerColor
						&&board.get(y+2).get(x+2).getColor()==playerColor
						&&board.get(y+3).get(x+3).getColor()==playerColor
						&&board.get(y).get(x).getColor()==Blank
						){
					return(x);		
			}
		}
			
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y+1).get(x-1).getColor()==playerColor
						&&board.get(y+2).get(x-2).getColor()==playerColor
						&&board.get(y+3).get(x-3).getColor()==playerColor
						&&board.get(y).get(x).getColor()==Blank
						){
					return(x);
				
			}
		}
		}
		return -1;
		
		


		
		
	}
	
	
	
	
	
	
	public int getValue(){
		// Human win
		// Value 1000* number of wins

		// Assume Player color = Red , AI color = YEllow
		Color playerColor = Color.RED;
		Color aiColor = Color.YELLOW;
		Color Blank = Color.WHITE;
		int HumanWin=-1;
		int numAIwins=0;


		/////////Check for Human win = 0
		////////

		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){

				// AI check if player got 3 vertical
				if(y+3<6&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y+1).get(x).getColor()==playerColor
						&&board.get(y+2).get(x).getColor()==playerColor
						&&board.get(y+3).get(x).getColor()==Blank
						){
					HumanWin=1;
				}
			}
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){

				//AI check if player got 3 horizontal RIGHT
				// O O O X
				if(		x+3<7
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y).get(x+1).getColor()==playerColor
						&&board.get(y).get(x+2).getColor()==playerColor
						&&board.get(y).get(x+3).getColor()==Blank
						){
					HumanWin=1;
				}
			}
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){

				//AI check if player got 3 horizontal left
				// X O O O
				if(		x+2<7&&x-1>=0
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y).get(x+1).getColor()==playerColor
						&&board.get(y).get(x+2).getColor()==playerColor
						&&board.get(y).get(x-1).getColor()==playerColor
						){
					HumanWin=1;
				}
			}
		}
		
		

		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       X        3 X
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y+1).get(x+1).getColor()==playerColor
						&&board.get(y+2).get(x+2).getColor()==playerColor
						&&board.get(y+3).get(x+3).getColor()==Blank
						&&board.get(y+2).get(x+3).getColor()!=Blank){
					HumanWin=1;	
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y+1).get(x-1).getColor()==playerColor
						&&board.get(y+2).get(x-2).getColor()==playerColor
						&&board.get(y+3).get(x-3).getColor()==Blank
						&&board.get(y+2).get(x-3).getColor()!=Blank){
					HumanWin=1;

				}
			}
		}

		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       O        3 O
		////////// 2     X          2   X
		////////// 1   O            1     O
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y+1).get(x+1).getColor()==playerColor
						&&board.get(y+3).get(x+3).getColor()==playerColor
						&&board.get(y+2).get(x+2).getColor()==Blank
						&&board.get(y+1).get(x+2).getColor()!=Blank){
					HumanWin=1;
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y+1).get(x-1).getColor()==playerColor
						&&board.get(y+3).get(x-3).getColor()==playerColor
						&&board.get(y+2).get(x-2).getColor()==Blank
						&&board.get(y+1).get(x-2).getColor()!=Blank){
					HumanWin=1;

				}
			}
		}

		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   X            1     X
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y+2).get(x+2).getColor()==playerColor
						&&board.get(y+3).get(x+3).getColor()==playerColor
						&&board.get(y+1).get(x+1).getColor()==Blank
						&&board.get(y).get(x+1).getColor()!=Blank){
					HumanWin=1;		
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==playerColor
						&&board.get(y+2).get(x-2).getColor()==playerColor
						&&board.get(y+3).get(x-3).getColor()==playerColor
						&&board.get(y+1).get(x-1).getColor()==Blank
						&&board.get(y).get(x-1).getColor()!=Blank){
					HumanWin=1;

				}
			}
		}

		/////////// BLOCK PLAYER DIAGONAL  TRAP (base case)
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 X              0       X
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y+1).get(x+1).getColor()==playerColor
						&&board.get(y+2).get(x+2).getColor()==playerColor
						&&board.get(y+3).get(x+3).getColor()==playerColor
						&&board.get(0).get(x).getColor()==Blank
						){
					HumanWin=1;	
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y+1).get(x-1).getColor()==playerColor
						&&board.get(y+2).get(x-2).getColor()==playerColor
						&&board.get(y+3).get(x-3).getColor()==playerColor
						&&board.get(0).get(x).getColor()==Blank
						){
					HumanWin=1;
				}
			}
		}



		///////////  BLOCK PLAYER DIAGONAL  TRAP 
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 X              0       X
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y+1).get(x+1).getColor()==playerColor
						&&board.get(y+2).get(x+2).getColor()==playerColor
						&&board.get(y+3).get(x+3).getColor()==playerColor
						&&board.get(y).get(x).getColor()==Blank
						){
					HumanWin=1;	
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y+1).get(x-1).getColor()==playerColor
						&&board.get(y+2).get(x-2).getColor()==playerColor
						&&board.get(y+3).get(x-3).getColor()==playerColor
						&&board.get(y).get(x).getColor()==Blank
						){
					HumanWin=1;

				}
			}
		}



		//////////!!!!!!!!

		///////////////////
		///////////////////
		///////////////////
		int num4wins=0;
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){

				// AI check if player got 4 vertical
				if(y+3<6&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x).getColor()==aiColor
						&&board.get(y+2).get(x).getColor()==aiColor
						&&board.get(y+3).get(x).getColor()==aiColor
						){
					return 5000;
				}
			}
		}
		for (int x=0;x<7;x++){  //BASE
			for(int y=0;y<6;y++){

				//AI check if player got 4 horizontal RIGHT
				// O O O X
				if(		x+3<7
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y).get(x+1).getColor()==aiColor
						&&board.get(y).get(x+2).getColor()==aiColor
						&&board.get(y).get(x+3).getColor()==aiColor
						){
					return 5000;
				}
			}
		}
		
		for (int x=0;x<7;x++){  
			for(int y=0;y<6;y++){

				
				// diagonal left
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x+1).getColor()==aiColor
						&&board.get(y+2).get(x+2).getColor()==aiColor
						&&board.get(y+3).get(x+3).getColor()==aiColor
						){
					return 5000;
				}
			}
		}
		
		for (int x=0;x<7;x++){  
			for(int y=0;y<6;y++){

				
				// diagonal left
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x-1).getColor()==aiColor
						&&board.get(y+2).get(x-2).getColor()==aiColor
						&&board.get(y+3).get(x-3).getColor()==aiColor
						){
					return 5000;
				}
			}
		}
		// special 1
		for (int x=0;x<7;x++){  
				if(x+3<7&&x-1>=0
				&&board.get(0).get(x).getColor()==aiColor
				&&board.get(0).get(x+1).getColor()==aiColor
				&&board.get(0).get(x+2).getColor()==aiColor
				&&board.get(0).get(x+3).getColor()==Blank
				&&board.get(0).get(x-1).getColor()==Blank
				){
					return 3000;
				}
			
		}

		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       X        3 X
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x+1).getColor()==aiColor
						&&board.get(y+2).get(x+2).getColor()==aiColor
						&&board.get(y+3).get(x+3).getColor()==aiColor
						&&board.get(y+2).get(x+3).getColor()!=Blank){
					return 5000;	
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x-1).getColor()==aiColor
						&&board.get(y+2).get(x-2).getColor()==aiColor
						&&board.get(y+3).get(x-3).getColor()==aiColor
						&&board.get(y+2).get(x-3).getColor()!=Blank){
					return 5000;

				}
			}
		}

		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       O        3 O
		////////// 2     X          2   X
		////////// 1   O            1     O
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x+1).getColor()==aiColor
						&&board.get(y+3).get(x+3).getColor()==aiColor
						&&board.get(y+2).get(x+2).getColor()==aiColor
						&&board.get(y+1).get(x+2).getColor()!=Blank){
					return 5000;
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x-1).getColor()==aiColor
						&&board.get(y+3).get(x-3).getColor()==aiColor
						&&board.get(y+2).get(x-2).getColor()==aiColor
						&&board.get(y+1).get(x-2).getColor()!=Blank){
					return 5000;

				}
			}
		}

		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   X            1     X
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+2).get(x+2).getColor()==aiColor
						&&board.get(y+3).get(x+3).getColor()==aiColor
						&&board.get(y+1).get(x+1).getColor()==aiColor
						&&board.get(y).get(x+1).getColor()!=Blank){
					return 5000;		
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+2).get(x-2).getColor()==aiColor
						&&board.get(y+3).get(x-3).getColor()==aiColor
						&&board.get(y+1).get(x-1).getColor()==aiColor
						&&board.get(y).get(x-1).getColor()!=Blank){
					return 5000;

				}
			}
		}

		/////////// BLOCK PLAYER DIAGONAL  TRAP (base case)
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 X              0       X
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y+1).get(x+1).getColor()==aiColor
						&&board.get(y+2).get(x+2).getColor()==aiColor
						&&board.get(y+3).get(x+3).getColor()==aiColor
						&&board.get(0).get(x).getColor()==aiColor
						){
					return 5000;	
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y+1).get(x-1).getColor()==aiColor
						&&board.get(y+2).get(x-2).getColor()==aiColor
						&&board.get(y+3).get(x-3).getColor()==aiColor
						&&board.get(0).get(x).getColor()==aiColor
						){
					return 5000;
				}
			}
		}



		///////////  BLOCK PLAYER DIAGONAL  TRAP 
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 X              0       X
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y+1).get(x+1).getColor()==aiColor
						&&board.get(y+2).get(x+2).getColor()==aiColor
						&&board.get(y+3).get(x+3).getColor()==aiColor
						&&board.get(y).get(x).getColor()==aiColor
						){
					return 5000;	
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y+1).get(x-1).getColor()==aiColor
						&&board.get(y+2).get(x-2).getColor()==aiColor
						&&board.get(y+3).get(x-3).getColor()==aiColor
						&&board.get(y).get(x).getColor()==aiColor
						){
					return 5000;

				}
			}
		}
		////////@#!@#!@#!@#!#@
		
		if (HumanWin==1){
			return 0;
		}

		
		

		///////////////////
		///////////////////
		///////////////////
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){

				// AI check if player got 3 vertical
				if(y+3<6&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x).getColor()==aiColor
						&&board.get(y+2).get(x).getColor()==aiColor
						&&board.get(y+3).get(x).getColor()==Blank
						){
					numAIwins++;
				}
			}
		}
		for (int x=0;x<7;x++){
//			for(int y=1;y<6;y++){

				//AI check if player got 3 horizontal RIGHT
				// O O O X
				if(		x+3<7
						&&board.get(0).get(x).getColor()==aiColor
						&&board.get(0).get(x+1).getColor()==aiColor
						&&board.get(0).get(x+2).getColor()==aiColor
						&&board.get(0).get(x+3).getColor()==Blank
						){
					numAIwins++;
				}
//			}
		}
		for (int x=0;x<7;x++){
//			for(int y=1;y<6;y++){

				//AI check if player got 3 horizontal left
				// X O O O
				if(		x+2<7&&x-1>=0
						&&board.get(0).get(x).getColor()==aiColor
						&&board.get(0).get(x+1).getColor()==aiColor
						&&board.get(0).get(x+2).getColor()==aiColor
						&&board.get(0).get(x-1).getColor()==aiColor
						){
					numAIwins++;
				}
//			}
		}
		for (int x=0;x<7;x++){

			// AI check if player make a trap O O X O  ON BASE
			if(		x+3<7
					&&board.get(0).get(x).getColor()==aiColor
					&&board.get(0).get(x+1).getColor()==aiColor
					&&board.get(0).get(x+2).getColor()==Blank
					&&board.get(0).get(x+3).getColor()==aiColor){
				numAIwins++;

			}
		}
		for (int x=0;x<7;x++){

			// AI check if player make a trap O X O O ON BASE
			if(		x+3<7
					&&board.get(0).get(x).getColor()==aiColor
					&&board.get(0).get(x+1).getColor()==Blank
					&&board.get(0).get(x+2).getColor()==aiColor
					&&board.get(0).get(x+3).getColor()==aiColor){
				numAIwins++;
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){

				// AI check if player make a trap O O X O  NOT ON BASE
				if(		x+3<7&&y-1>=0
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y).get(x+1).getColor()==aiColor
						&&board.get(y).get(x+2).getColor()==Blank
						&&board.get(y).get(x+3).getColor()==aiColor
						&&board.get(y-1).get(x+2).getColor()!=Blank){
					numAIwins++;
				}
			}
		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				// AI check if player make a trap O X O O NOT ON BASE
				if(		x+3<7&&y-1>=0
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y).get(x+1).getColor()==Blank
						&&board.get(y).get(x+2).getColor()==aiColor
						&&board.get(y).get(x+3).getColor()==aiColor
						&&board.get(y-1).get(x+1).getColor()!=Blank){
					numAIwins++;
				}
			}
		}










		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       X        3 X
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x+1).getColor()==aiColor
						&&board.get(y+2).get(x+2).getColor()==aiColor
						&&board.get(y+3).get(x+3).getColor()==Blank
						&&board.get(y+2).get(x+3).getColor()!=Blank){
					numAIwins++;	
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x-1).getColor()==aiColor
						&&board.get(y+2).get(x-2).getColor()==aiColor
						&&board.get(y+3).get(x-3).getColor()==Blank
						&&board.get(y+2).get(x-3).getColor()!=Blank){
					numAIwins++;

				}
			}
		}

		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       O        3 O
		////////// 2     X          2   X
		////////// 1   O            1     O
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x+1).getColor()==aiColor
						&&board.get(y+3).get(x+3).getColor()==aiColor
						&&board.get(y+2).get(x+2).getColor()==Blank
						&&board.get(y+1).get(x+2).getColor()!=Blank){
					numAIwins++;
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x-1).getColor()==aiColor
						&&board.get(y+3).get(x-3).getColor()==aiColor
						&&board.get(y+2).get(x-2).getColor()==Blank
						&&board.get(y+1).get(x-2).getColor()!=Blank){
					numAIwins++;

				}
			}
		}

		///////////  BLOCK PLAYER DIAGONAL  TRAP
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   X            1     X
		////////// 0 O              0       O
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+2).get(x+2).getColor()==aiColor
						&&board.get(y+3).get(x+3).getColor()==aiColor
						&&board.get(y+1).get(x+1).getColor()==Blank
						&&board.get(y).get(x+1).getColor()!=Blank){
					numAIwins++;		
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+2).get(x-2).getColor()==aiColor
						&&board.get(y+3).get(x-3).getColor()==aiColor
						&&board.get(y+1).get(x-1).getColor()==Blank
						&&board.get(y).get(x-1).getColor()!=Blank){
					numAIwins++;

				}
			}
		}

		/////////// BLOCK PLAYER DIAGONAL  TRAP (base case)
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 X              0       X
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y+1).get(x+1).getColor()==aiColor
						&&board.get(y+2).get(x+2).getColor()==aiColor
						&&board.get(y+3).get(x+3).getColor()==aiColor
						&&board.get(0).get(x).getColor()==Blank
						){
					numAIwins++;	
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y+1).get(x-1).getColor()==aiColor
						&&board.get(y+2).get(x-2).getColor()==aiColor
						&&board.get(y+3).get(x-3).getColor()==aiColor
						&&board.get(0).get(x).getColor()==Blank
						){
					numAIwins++;
				}
			}
		}



		///////////  BLOCK PLAYER DIAGONAL  TRAP 
		////////// 3       O        3 O
		////////// 2     O          2   O
		////////// 1   O            1     O
		////////// 0 X              0       X
		//////////   0 1 2 3          0 1 2 3
		//////////  done
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y+1).get(x+1).getColor()==aiColor
						&&board.get(y+2).get(x+2).getColor()==aiColor
						&&board.get(y+3).get(x+3).getColor()==aiColor
						&&board.get(y).get(x).getColor()==Blank
						){
					numAIwins++;	
				}
			}

		}
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y+1).get(x-1).getColor()==aiColor
						&&board.get(y+2).get(x-2).getColor()==aiColor
						&&board.get(y+3).get(x-3).getColor()==aiColor
						&&board.get(y).get(x).getColor()==Blank
						){
					numAIwins++;

				}
			}
		}
		//		if (numAIwins!=0){
		//			return numAIwins*1000;
		//		}


		////////////////////Check for 2 in a row = 50*num

		int numOf2=0;

		//VERTICAL
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x).getColor()==aiColor
						&&board.get(y+2).get(x).getColor()==Blank
						&&board.get(y+3).get(x).getColor()==Blank
						){
					numOf2++;	
				}
			}
		}

		//HORIZONTAL RIGHT ON  BASE
		for (int x=0;x<7;x++){
			if(		x+3<7
					&&board.get(0).get(x).getColor()==aiColor
					&&board.get(0).get(x+1).getColor()==aiColor
					&&board.get(0).get(x+2).getColor()==Blank
					&&board.get(0).get(x+3).getColor()==Blank
					){
				numOf2++;	
			}

		}
		//HORIZONTAL LEFT ON BASE
		for (int x=0;x<7;x++){

			if(		x-2>=0&x+1<7
					&&board.get(0).get(x).getColor()==aiColor
					&&board.get(0).get(x+1).getColor()==aiColor
					&&board.get(0).get(x-1).getColor()==Blank
					&&board.get(0).get(x-2).getColor()==Blank
					){
				numOf2++;	
			}

		}

		//HORIZONTAL RIGHT NOT ON BASE
		for (int x=0;x<7;x++){
			for(int y=1;y<6;y++){
				if(		x+2<7
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y).get(x+1).getColor()==aiColor
						&&board.get(y).get(x+2).getColor()==Blank
						&&board.get(y-1).get(x+2).getColor()!=Blank
						){
					numOf2++;	
				}
			}
		}

		//HORIZONTAL LEFT NOT ON BASE
		for (int x=0;x<7;x++){
			for(int y=1;y<6;y++){
				if(		x-1>=0&&x+1<7
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y).get(x+1).getColor()==aiColor
						&&board.get(y).get(x-1).getColor()==Blank
						&&board.get(y-1).get(x-1).getColor()!=Blank
						){
					numOf2++;	
				}
			}
		}


		// LEFT Diagonal 2
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x-1).getColor()==aiColor
						&&board.get(y+2).get(x-2).getColor()==Blank
						&&board.get(y+3).get(x-3).getColor()==Blank
						){
					numOf2++;	
				}
			}
		}
		// RIGHT Diagonal 2
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x+1).getColor()==aiColor
						&&board.get(y+2).get(x+2).getColor()==Blank
						&&board.get(y+3).get(x+3).getColor()==Blank
						){
					numOf2++;	
				}
			}
		}





		//		
		//		if (numOf2!=0){
		//			return numOf2*50;
		//		}
		//		

		int numOf1=0;

		//VERTICAL UP
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x).getColor()==Blank
						&&board.get(y+2).get(x).getColor()==Blank
						&&board.get(y+3).get(x).getColor()==Blank
						){
					numOf1++;	
				}
			}
		}
		// LEFT ON BASE
		for (int x=0;x<7;x++){

			if(		x-3>=0
					&&board.get(0).get(x).getColor()==aiColor
					&&board.get(0).get(x-1).getColor()==Blank
					&&board.get(0).get(x-2).getColor()==Blank
					&&board.get(0).get(x-3).getColor()==Blank
					){
				numOf1++;	
			}

		}
		//RIGHT ON BASE
		for (int x=0;x<7;x++){

			if(		x+3<7
					&&board.get(0).get(x).getColor()==aiColor
					&&board.get(0).get(x+1).getColor()==Blank
					&&board.get(0).get(x+2).getColor()==Blank
					&&board.get(0).get(x+3).getColor()==Blank
					){
				numOf1++;	
			}

		}

		// LEFT
		for (int x=0;x<7;x++){
			for(int y=1;y<6;y++){
				if(		x-3>=0
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y).get(x-1).getColor()==Blank
						&&board.get(y).get(x-2).getColor()==Blank
						&&board.get(y).get(x-3).getColor()==Blank
						&&board.get(y-1).get(x-1).getColor()!=Blank
						){
					numOf1++;	
				}
			}
		}
		//RIGHT
		for (int x=0;x<7;x++){
			for(int y=1;y<6;y++){
				if(		x+3<7
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y).get(x+1).getColor()==Blank
						&&board.get(y).get(x+2).getColor()==Blank
						&&board.get(y).get(x+3).getColor()==Blank
						&&board.get(y-1).get(x+1).getColor()!=Blank

						){
					numOf1++;	
				}
			}
		}


		// LEFT Diagonal
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x-3>=0
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x-1).getColor()==Blank
						&&board.get(y+2).get(x-2).getColor()==Blank
						&&board.get(y+3).get(x-3).getColor()==Blank
						){
					numOf1++;	
				}
			}
		}
		// RIGHT Diagonal
		for (int x=0;x<7;x++){
			for(int y=0;y<6;y++){
				if(		y+3<6&&x+3<7
						&&board.get(y).get(x).getColor()==aiColor
						&&board.get(y+1).get(x+1).getColor()==Blank
						&&board.get(y+2).get(x+2).getColor()==Blank
						&&board.get(y+3).get(x+3).getColor()==Blank
						){
					numOf1++;	
				}
			}
		}


		return numAIwins*1000+numOf1*10+numOf2*50;

	}

	
	
}