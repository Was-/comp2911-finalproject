
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.FileInputStream;

//import sun.audio.AudioData;
//import sun.audio.AudioPlayer;
//import sun.audio.AudioStream;
//import sun.audio.ContinuousAudioDataStream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class StandardGame extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame mainFrame = new JFrame("Connect4Panel");;
	private JPanel gamePanel =  new JPanel();
	private JPanel gameOption  = new JPanel(new GridLayout());
	private JPanel gameType;
	private final Board gameState;	
	
	
	public StandardGame (final GameEngine gameManager ) {
		//Connect4Panel gameType;
			
		this.gameState = gameManager.getGameBoard();
		gamePanel.setBackground(new Color(23, 54, 93));
		// create the connect4 panel

		SideBar info = new SideBar(gameState, gamePanel.getBackground());
		 
		switch (gameManager.getMode()) {
		case 0: gameType = new Connect4Panel(gameManager, info); break;
		case 1: gameType = new InvisibleModePanel(gameManager, info); break;
		case 2: gameType = new TimerModePanel(gameManager, info); break;
		default: gameType = new Connect4Panel(gameManager, info); break;
		}
		
		gameType.addMouseListener((MouseListener) gameType);
		gameType.addMouseMotionListener((MouseMotionListener) gameType);
		
		// Create a new button and add the action listener.
		
		JButton instruction = new JButton("Instruction");
		JButton onePlayer = new JButton("1 Player");
		JButton twoPlayer = new JButton("2 Player");
		
		twoPlayer.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				gameState.clearBoard();
	    		mainFrame.repaint();
			}
		});
		
		gameOption.add(instruction);
		gameOption.add(onePlayer);
		gameOption.add(twoPlayer);
		
		//ConnectFour c = new Connect4Panel(gameManager, info);
		//InvisibleModePanel c = new InvisibleModePanel(gameType);
		//c.setLayout(new BorderLayout());
		//c.add(gameType, BorderLayout.CENTER);
		gamePanel.add(gameType, BorderLayout.CENTER);
		gamePanel.add(info, BorderLayout.EAST);
		this.setPreferredSize(new Dimension(570,540));
		this.setLayout(new BorderLayout());
		this.add(gamePanel,BorderLayout.CENTER);
		this.add(gameOption,BorderLayout.SOUTH);
	}
}