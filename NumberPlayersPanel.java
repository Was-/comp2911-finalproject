import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;


public class NumberPlayersPanel extends JPanel implements ActionListener {
	JPanel heading;
	JPanel body;
	JPanel left;
	JPanel center;
	JPanel bottom;
	JPanel bottomCenter;
	JPanel right;
	JButton singlePlayer;
	JButton proceed;
	JButton pvp;
	JButton backToMain;	
	GameEngine gameManager;
	
	public NumberPlayersPanel (final GameEngine gameManager) {
		this.gameManager = gameManager;
		
		heading = new JPanel(new BorderLayout());
		body = new JPanel(new GridLayout(1, 3));
		left = new JPanel();
		center = new JPanel(new GridLayout(10, 1));
		right = new JPanel(new BorderLayout());
		bottom = new JPanel(new GridLayout(1, 3));
		proceed = new JButton("Proceed to Game");
		bottomCenter = new JPanel(new GridLayout(3,1));
		JPanel radioHolder = new JPanel(new FlowLayout());
		this.setLayout(new BorderLayout());
		
		singlePlayer = new JButton("Single Player");
		pvp = new JButton ("Player vs Player");
		backToMain = new JButton("Back to Main Menu");
		
		ImageIcon icon = new ImageIcon("connect4Heading.png");
		JLabel logo = new JLabel("", icon, JLabel.CENTER);
		heading.add(logo, BorderLayout.CENTER);
		heading.setBackground(Color.YELLOW);
		heading.setPreferredSize(new Dimension(this.getWidth(),100));
				
		body.add(left);
		body.add(center);
		body.add(right);
		
		left.setBackground(Color.YELLOW);

		
		center.add(Box.createRigidArea(new Dimension(1,0)));
		//center.add(singlePlayer);
		//center.add(Box.createRigidArea(new Dimension(1,0)));
		//center.add(pvp);
		String[] petStrings = { "Single Player", "Player vs Player"};
		JComboBox numPlayers = new JComboBox(petStrings);
		numPlayers.setSelectedIndex(1);
		center.add(numPlayers);
		center.add(Box.createRigidArea(new Dimension(1,0)));
		center.setBackground(Color.YELLOW);

		
		JPanel bottomLeft = new JPanel();
		bottomLeft.setBackground(Color.YELLOW);
		JPanel bottomRight = new JPanel();
		bottomRight.setBackground(Color.YELLOW);

		final JRadioButton lvl1 = new JRadioButton("LvL 1");
		final JRadioButton lvl2 = new JRadioButton("LvL 2");
		radioHolder.add(lvl1);
		radioHolder.add(lvl2);
		radioHolder.setBackground(Color.YELLOW);
//		bottomCenter.add(new JLabel("Please select AI difficulty"));
//		bottomCenter.add(radioHolder);
//		bottomCenter.add(proceed);
//		bottom.setBackground(Color.YELLOW);
//		
		final JLabel onePlayerText = new JLabel("Please select AI difficulty");
		center.add(Box.createRigidArea(new Dimension(1,0)));
		center.add(onePlayerText);
		center.add(radioHolder);
		center.add(Box.createRigidArea(new Dimension(1,0)));
		center.add(proceed);
		
		//onePlayerText.setVisible(false);
		//radioHolder.setVisible(false);
		onePlayerText.setEnabled(false);
		lvl1.setEnabled(false);
		lvl2.setEnabled(false);
		
		//backToMain.setPreferredSize(new Dimension(backToMain.getWidth()/1, backToMain.getHeight()));
		right.add(backToMain, BorderLayout.SOUTH);
		right.setBackground(Color.YELLOW);
		
		this.add(heading, BorderLayout.NORTH);
		this.add(body, BorderLayout.CENTER);
		//this.add(bottom, BorderLayout.SOUTH);

		numPlayers.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	        	JComboBox cb = (JComboBox)e.getSource();
	            String numPlayer = (String)cb.getSelectedItem();
	            gameManager.setMode(0);
	        	if (numPlayer.equals("Single Player")) {
	        		onePlayerText.setEnabled(true);
	        		lvl1.setEnabled(true);
	        		lvl2.setEnabled(true);
	        		gameManager.setNumHumanPlayers(1);
	        	} else {
	        		onePlayerText.setEnabled(false);
	        		lvl1.setEnabled(false);
	        		lvl2.setEnabled(false);
	        		gameManager.setNumHumanPlayers(2);
	        	}
	        }
	      });
		
		lvl1.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        	lvl2.setSelected(false);
	        	gameManager.player2 = new SimpleAI(1,Color.YELLOW,gameManager.getGameBoard());
                gameManager.setAI(1);
	        }
	      });
		
		
		lvl2.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        	lvl1.setSelected(false);
	        	 gameManager.player2 = new IntermediateAI(1,Color.YELLOW,gameManager.getGameBoard());
             	 gameManager.setAI(2);
	        }
	      });
		
		proceed.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        	gameManager.getJFrame().getContentPane().remove(gameManager.getCurrPanel());
	        	gameManager.setCurrPanel(new StandardGame(gameManager));
	        	gameManager.getJFrame().getContentPane().add(gameManager.getCurrPanel(), BorderLayout.CENTER);	        	
	        	gameManager.getJFrame().getContentPane().validate();
	        }
	      });
		
		backToMain.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        	gameManager.getJFrame().getContentPane().remove(gameManager.getCurrPanel());
	        	gameManager.setCurrPanel(gameManager.getMainMenuPanel());
	        	gameManager.getJFrame().getContentPane().add(gameManager.getCurrPanel(), BorderLayout.CENTER);	        	
	        	gameManager.getJFrame().getContentPane().validate();
	        }
	      });
		
	}
	
	public Dimension getPreferredSize() {
        return new Dimension(650,450);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
