import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Connect4Panel extends JPanel implements MouseListener, MouseMotionListener, 
					ConnectFour, KeyListener{

	private int dimension = 60;
	private Board gameState;
	private SideBar sideBar;
	private Color highlightYellow = new Color(250, 255, 114), hightlightRed = new Color(244, 121, 131);
	private GameEngine gameManager;
	private ArrayList<ArrayList<DisplayCounter>> dCounters;
	
	public Connect4Panel (GameEngine gameManager, SideBar sideBar) {
		this.gameManager = gameManager;
		this.gameState = gameManager.getGameBoard();
		this.sideBar = sideBar;
		setBackground(new Color(0, 176, 255));
		setLayout(new GridLayout(6, 7));
		setPreferredSize(new Dimension(490, 420));
//		setPreferredSize(new Dimension(630, 540));
		setFocusable(true);
		
		
		dCounters = new ArrayList<ArrayList<DisplayCounter>>();
        for (int i=0;i<6;i++) {
            dCounters.add(new ArrayList<DisplayCounter>());
            for (int j=0;j<7;j++) {
                dCounters.get(i).add(new DisplayCounter(Color.WHITE, j, i));
            }
        }		
        
        JButton newGame = new JButton("NewGame");
        JButton back = new JButton("Back");
        JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(1,2));
		buttonPanel.add(newGame);
		buttonPanel.add(back);
		
		
		sideBar.add(buttonPanel,BorderLayout.SOUTH);
		

		
		newGame.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				gameState.clearBoard();
				CleanBoard();
				
			}
			
		});
		
		back.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
        
		paint();
	}

	public void paintComponent(Graphics g) {
        super.paintComponent(g);       	
        paint();
    }
	
	public Board getGameState () {
		return gameState;
	}
	
	
	public void paint(){
        dimension = getSize().height/6 > getSize().width/7 ? getSize().width/7 : getSize().height/6;
        
//        for (int row = 5; row >= 0; row--) {
//        	for (int col = 0; col < 7; col++) {
//    			Counter counter = gameState.getCounter(col, row);
//    			counter.setSize(dimension);
//    			add(counter);
//        	}
//        }
        
        for (int row = 5; row >= 0; row--) {
            for (int col = 0; col < 7; col++) {
                DisplayCounter counter = dCounters.get(row).get(col);
                counter.setSize(dimension);
                add(counter);
            }
        }
        
	}
	
	public int getCounterDimension () {
		return dimension;
	}
	
	public GameEngine getGameEngine () {
		return gameManager;
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		mouseMoved(arg0);
	}

	@Override
	public void mouseMoved(MouseEvent e) { //catch exception
		if (e.getX() < 0 || e.getX() > getWidth() || e.getY() < 0 || e.getY() > getHeight()) return;
		int buffer = 3, posCheck = e.getX() % dimension;
		if (posCheck < buffer || posCheck + buffer > dimension)
			return;
		int highlight = e.getX()/dimension;
		Color colour = gameState.getRoundNum() % 2 == 0? hightlightRed : highlightYellow;
        for (int row = 5; row >= 0; row--){
        	for (int col = 0; col < 7; col++)
        		if (col == highlight)
        			dCounters.get(row).get(col).highlights(colour);
        		else
        			dCounters.get(row).get(col).removeHighlighter();
        }
        repaint();
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
		for (int row = 5; row >= 0; row--){
        	for (int col = 0; col < 7; col++)
    			dCounters.get(row).get(col).removeHighlighter();
        }
		repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.getX() < 0 || e.getX() > getWidth() || e.getY() < 0 || e.getY() > getHeight()) return;
		int buffer = 3, col = e.getX()/dimension;
		if (e.getX()%dimension < buffer || e.getX()%dimension + buffer > dimension) return;
		int row = gameState.getRow(col);
		
		
		boolean AIturn;
		if(gameState.getRoundNum()%2==0){
			AIturn = makeMove(col, Color.RED);
		}
		else {
			AIturn = makeMove(col,Color.YELLOW);
		}

		if(AIturn && (gameManager.getAI()==1 || gameManager.getAI()==2)){ // 1 for Simple AI , 2 for Intermediate AI
			int move = gameManager.getPlayer2().getMove();
			makeMove(move,Color.YELLOW);
		}
//		makeMove(col, gameState.getBoard().get(row).get(col).getColor());
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		int col = Character.getNumericValue(arg0.getKeyChar());
		int row = gameState.getRow(col);
		Color c = gameState.getBoard().get(row).get(col).getColor();
		switch(arg0.getKeyChar()){
		case '1': makeMove(0, c); break;
		case '2': makeMove(1, c); break;
		case '3': makeMove(2, c); break;
		case '4': makeMove(3, c); break;
		case '5': makeMove(4, c); break;
		case '6': makeMove(5, c); break;
		case '7': makeMove(6, c); break;
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}
	
	public void setDisplayCounter (Color c, int row, int col) {
		dCounters.get(row).get(col).setColor(c);
	}
	
	public boolean makeMove(int col, Color c){
//		System.out.printf("makeMove(%d,%s);\n",col,c.toString());
		int row = gameState.getRow(col);
		if(!gameState.makeMove(col)) {
			JOptionPane.showMessageDialog(null, "You cannot place a counter there, please choose a different position.");
			return false;
		}
			
        setDisplayCounter(c, row, col);
		repaint();
		sideBar.paintComponent(gameState);
		sideBar.repaint();
		int win = gameState.checkForWin(col);

	
		if (win == 1) {
			if(gameManager.getAI()==1 && gameState.findLastPlayerID()==2){
				JOptionPane.showMessageDialog(null, " ! \nWould you like to play again?");
				
			}
			
			else {JOptionPane.showMessageDialog(null, "Congratulations Player "+gameState.findLastPlayerID()+
												", you have won! \nWould you like to play again?");
			}
			gameState.clearBoard();
			CleanBoard();
			repaint();
			return false;
		} 
		
		if (win == -1) {
			JOptionPane.showMessageDialog(null, "Both players have drawn! \nWould you like to play again?");
			gameState.clearBoard();
			CleanBoard();
			repaint();
			return false;
		}
		
		return true;
		
		}
	
	public void CleanBoard(){
			for(int x=0;x<7;x++){
				for(int y=0;y<6;y++){
	        setDisplayCounter(Color.WHITE, y, x);
			repaint();
			sideBar.paintComponent(gameState);
			sideBar.repaint();
				}
			}
	}
  			

		
	}


		


