import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class SelectModePanel extends JPanel implements ActionListener {
	JPanel heading;
	JPanel body;
	JPanel left;
	JPanel center;
	JPanel right;
	JButton invMode;
	JButton timerMode;
	JButton hintMode;	
	
	public SelectModePanel (final GameEngine gameManager) {
		heading = new JPanel();
		body = new JPanel(new BorderLayout());
		left = new JPanel();
		center = new JPanel(new GridLayout(2, 1));
		right = new JPanel();
		this.setLayout(new BorderLayout());
		
		invMode = new JButton("Invisible Mode");
		timerMode = new JButton("Timer Mode");
		hintMode = new JButton("Hint Mode");
		
		
		heading.setBackground(Color.DARK_GRAY);
		heading.setPreferredSize(new Dimension(this.getWidth(), 
								this.getPreferredSize().height/4));
				
		body.add(left, BorderLayout.WEST);
		body.add(center, BorderLayout.CENTER);
		body.add(right, BorderLayout.EAST);
		
		center.add(Box.createRigidArea(new Dimension(1,0)));
		center.add(invMode);
		center.add(Box.createRigidArea(new Dimension(1,0)));
		center.add(timerMode);
		center.add(Box.createRigidArea(new Dimension(1,0)));
		center.add(hintMode);
		//backToMain.setPreferredSize(new Dimension(backToMain.getWidth()/1, backToMain.getHeight()));

		this.add(heading, BorderLayout.NORTH);
		this.add(body, BorderLayout.CENTER);
		
		invMode.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        	gameManager.setMode(1);
	        	gameManager.getJFrame().getContentPane().remove(gameManager.getCurrPanel());
	        	gameManager.setCurrPanel(new StandardGame(gameManager));
	        	gameManager.getJFrame().getContentPane().add(gameManager.getCurrPanel(), BorderLayout.CENTER);	        	
	        	gameManager.getJFrame().getContentPane().validate();	        	
	        }
	      });
		
		timerMode.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        	gameManager.getJFrame().getContentPane().remove(gameManager.getCurrPanel());
	        	gameManager.setCurrPanel(new StandardGame(gameManager));
	        	gameManager.getJFrame().getContentPane().add(gameManager.getCurrPanel(), BorderLayout.CENTER);	        	
	        	gameManager.getJFrame().getContentPane().validate();	        	
	        }
	      });
		
		hintMode.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        	gameManager.getJFrame().getContentPane().remove(gameManager.getCurrPanel());
	        	gameManager.setCurrPanel(new StandardGame(gameManager));
	        	gameManager.getJFrame().getContentPane().add(gameManager.getCurrPanel(), BorderLayout.CENTER);	        	
	        	gameManager.getJFrame().getContentPane().validate();	        	

	        }
	      });
	}
	
	public Dimension getPreferredSize() {
        return new Dimension(650,450);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
