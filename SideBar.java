import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class SideBar extends JPanel {
	
//	private Board gameState;
    private	JLabel currentPlayer;
    private JLabel round;
    private JLabel timeRemaining;
    private Font font = new Font("Cabri", Font.BOLD, 24);
    
    private JButton newGame = new JButton("NewGame");
    private JButton back = new JButton("Back");
    
    private JPanel infoPanel = new JPanel();
    private JPanel buttonPanel = new JPanel();

    //private JPanel sideBar;
    
	public SideBar (final Board gameState, Color background) {
	

		setBackground(background);
		currentPlayer = new JLabel("Current Player " + gameState.findLastPlayerID());
		currentPlayer.setFont(font);
		currentPlayer.setForeground(Color.cyan);
		round = new JLabel("round: " + gameState.getRoundNum());
			round.setFont(font);
			round.setForeground(Color.cyan);
		timeRemaining = new JLabel("Time Left: " + gameState.getLastMove());
		timeRemaining.setFont(font);
		timeRemaining.setForeground(Color.cyan);
//		add(currentPlayer);
//		add(round);
//		add(timeRemaining);
		setLayout(new BorderLayout());
		add(infoPanel,BorderLayout.CENTER);
//		add(buttonPanel,BorderLayout.SOUTH);
		
		infoPanel.setLayout(new GridLayout(2,1));
		infoPanel.add(currentPlayer);
		infoPanel.add(round);
		infoPanel.setBackground(new Color(23, 54, 93));
		

	}
	
	

	public Dimension getPreferredSize() {
        return new Dimension(230,210);
    }
	
	public void paintComponent(Board gameState){
		currentPlayer.setText("Current Player: " + gameState.findLastPlayerID());
		//round.setText("round: " + gameState.getRoundNum());
		timeRemaining.setText("Time Remaining" + gameState.getLastMove());
		repaint();
	}
	
	
	
}
