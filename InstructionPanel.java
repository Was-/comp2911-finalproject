import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class InstructionPanel extends JPanel implements ActionListener {
	JPanel left;
	JPanel right;
	JPanel center;
	JPanel outerMost;
	JPanel title;
	JPanel body;
	JPanel currInstrucPanel;
	
	public InstructionPanel (final GameEngine gameManager) {
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.setBackground(new Color(0, 200, 255));
		
		title = new JPanel(new BorderLayout());
		title.setBackground(new Color(200, 0, 255));
		title.setPreferredSize(new Dimension(this.getWidth(), this.getHeight()/5));
		
		body = new JPanel(new FlowLayout());
		body.setBackground(new Color(0, 200, 255));
		body.setPreferredSize(new Dimension(this.getWidth(), 
				this.getHeight()- title.getHeight()));
		
		this.add(title);
		this.add(body);			
		left = new JPanel();
		center = new JPanel();
		center.setPreferredSize(new Dimension(this.getWidth()/3, body.getHeight()));

		right = new JPanel(new GridLayout(1, 15));
		right.setPreferredSize(new Dimension(this.getWidth()/3, body.getHeight()));

		
		body.add(left);		
		left.setBackground(new Color(255, 255, 0));
		left.setPreferredSize(new Dimension(this.getWidth()/3, body.getHeight()));
		
		body.add(center);
		body.add(right);
		right.setBackground(new Color(0, 0, 255));
		body.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		
		// center panel setup
		center.setBackground(new Color(0, 200, 255));
		// center.setPreferredSize(new Dimension(body.getHeight(), this.getWidth()));
		center.setLayout(new GridLayout(10, 3));
		center.add(Box.createRigidArea(new Dimension(1,0)));
	}
	
	public Dimension getPreferredSize() {
        return new Dimension(650,450);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
