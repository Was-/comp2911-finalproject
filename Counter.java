import java.awt.Color;

public class Counter {
    
    private Color c;
    private int row;
    private int col;
    
    public Counter(Color newC, int newRow, int newCol) {
        c = newC;
        row = newRow;
        col = newCol;
    }
    
    public Color getColor() {
        return c;
    }
    
    public void setColor(Color newC) {
        c = newC;
    }
    
    public int getRow() {
        return row;
    }
    
    public int getCol() {
        return col;
    }
    
    @Override
    public String toString() {
        String s;
        if (c.equals(Color.RED)) {
            s = "R";
        } else if (c.equals(Color.YELLOW)) {
            s = "Y";
        } else {
            s = " ";
        }
        return s;
    }
}