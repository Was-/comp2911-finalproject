import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class DisplayCounter extends JPanel {
    
    private int interval, size;
    private Color fill, highlight;
    private int col;
    private int row;

    public DisplayCounter(Color colour, int col, int row) {
        fill = colour;
        this.col = col;
        this.row = row;

    }
    
    public Color getColor() {
        return fill;
    }
    
    public void setSize(int dimension){
        size = (int)(dimension * 0.8);
        interval  = (int)(dimension * 0.1);
    }
    
    public void setColor(Color newC) {
        fill = newC;
    }
    
    // @Override
    // public String toString() {
    //     String s;
    //     if (fill.equals(Color.RED)) {
    //         s = "R";
    //     } else if (fill.equals(Color.YELLOW)) {
    //         s = "Y";
    //     } else {
    //         s = " ";
    //     }
    //     return s;
    // }
    
    public void paintComponent(Graphics g){
        g.setColor(fill);
        g.fillOval(interval, interval, size, size);
        if (highlight != null){
            g.setColor(highlight);
            g.fillRect(0, 0, size + 2*interval, size + 2*interval);
        }
    }
    
    public void highlights(Color colour){
        highlight = new Color(colour.getRed(), colour.getGreen(), colour.getBlue(), 90);
    }
    
    public void removeHighlighter(){
        highlight = null;
    }

}